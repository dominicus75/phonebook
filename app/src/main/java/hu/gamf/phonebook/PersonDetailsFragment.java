package hu.gamf.phonebook;

import android.os.Build;
import android.os.Bundle;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Objects;

import static java.lang.Integer.*;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link PersonDetailsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PersonDetailsFragment extends Fragment {

    public PersonDetailsFragment() {
        // Required empty public constructor
    }

    public static PersonDetailsFragment newInstance() {
        return new PersonDetailsFragment();
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_person_details, container, false);
        Person person = ((MainActivity) Objects.requireNonNull(getActivity())).getSelectedPerson();

        ImageView avatar = rootView.findViewById(R.id.avatar);
        int res = getActivity().getResources().getIdentifier(
                person.getAvatar(),
                "mipmap",
                getContext().getPackageName()
        );
        avatar.setImageResource(res);
        TextView name = rootView.findViewById(R.id.name);
        name.setText(person.getFullName());
        TextView phone = rootView.findViewById(R.id.phone);
        phone.setText(person.getPhone());
        TextView email = rootView.findViewById(R.id.email);
        email.setText(person.getEmail());

        return rootView;

    }
}