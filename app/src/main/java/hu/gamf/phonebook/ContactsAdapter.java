package hu.gamf.phonebook;

import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;


public class ContactsAdapter extends RecyclerView.Adapter<ContactsAdapter.PersonHolder> {

    private ContactListFragment contactListFragment;
    private List<Person> contactList;

    public ContactsAdapter(ContactListFragment contactListFragment, List<Person> contactList) {
        this.contactListFragment = contactListFragment;
        this.contactList = contactList;
    }

    @NonNull
    @Override
    public PersonHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.contact_list_row, parent, false);
        return new PersonHolder(itemView);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onBindViewHolder(@NonNull PersonHolder holder, int position) {
        final Person person = contactList.get(position);
        int res = holder.avatar.getResources().getIdentifier(
                contactList.get(position).getAvatar(),
                "mipmap",
                "hu.gamf.phonebook"
        );
        holder.avatar.setImageResource(res);
        holder.name.setText(person.getFullName());
        holder.phone.setText(person.getPhone());
        holder.email.setText(person.getEmail());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                contactListFragment.selectItemAndNavigate(person);
            }
        });
    }

    @Override
    public int getItemCount() {
        return contactList.size();
    }

    public class PersonHolder extends RecyclerView.ViewHolder {

        public ImageView avatar;
        public TextView name;
        public TextView phone;
        public TextView email;

        public PersonHolder(View itemView) {
            super(itemView);
            avatar = (ImageView) itemView.findViewById(R.id.avatar);
            name   = (TextView) itemView.findViewById(R.id.name);
            phone  = (TextView) itemView.findViewById(R.id.phone);
            email  = (TextView) itemView.findViewById(R.id.email);
        }
    }

}
