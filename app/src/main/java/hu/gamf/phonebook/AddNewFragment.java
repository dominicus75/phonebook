package hu.gamf.phonebook;

import android.os.Build;
import android.os.Bundle;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import java.util.Objects;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link AddNewFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AddNewFragment extends Fragment {

    public AddNewFragment() {
        // Required empty public constructor
    }

    public static AddNewFragment newInstance() {
        AddNewFragment fragment = new AddNewFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        OnBackPressedCallback callback = new OnBackPressedCallback(true) {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void handleOnBackPressed() {
                Fragment fragment = ContactListFragment.newInstance();
                ((MainActivity) Objects.requireNonNull(getActivity())).loadFragment(
                        fragment,
                        "contacts",
                        false
                );
            }
        };
        requireActivity().getOnBackPressedDispatcher().addCallback(this, callback);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View rootView = inflater.inflate(R.layout.fragment_add_new, container, false);

        final Button add = (Button)rootView.findViewById(R.id.add);
        add.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View v) {
                EditText firstEdit = rootView.findViewById(R.id.editTextFirst);
                String first = firstEdit.getText().toString();

                EditText lastEdit = rootView.findViewById(R.id.editTextLast);
                String last = lastEdit.getText().toString();

                EditText phoneEdit = rootView.findViewById(R.id.editTextPhone);
                String phone = phoneEdit.getText().toString();

                EditText emailEdit = rootView.findViewById(R.id.editTextEmail);
                String email = emailEdit.getText().toString();

                ((MainActivity) Objects.requireNonNull(getActivity())).addContact(
                        first, last, phone, email
                );

                Fragment fragment = ContactListFragment.newInstance();
                ((MainActivity) Objects.requireNonNull(getActivity())).loadFragment(
                        fragment,
                        "contacts",
                        false
                );

            }
        });

        return rootView;
    }

}