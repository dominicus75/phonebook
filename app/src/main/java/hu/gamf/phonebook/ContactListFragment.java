package hu.gamf.phonebook;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.util.List;
import java.util.Objects;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ContactListFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ContactListFragment extends Fragment implements View.OnClickListener {

    public ContactListFragment() {
        // Required empty public constructor
    }

    public static ContactListFragment newInstance() {
        return new ContactListFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_contact_list, container, false);
        Button addNewButton = (Button)rootView.findViewById(R.id.addNewContactButton);
        addNewButton.setOnClickListener(this);
        List<Person> contactList = ((MainActivity) Objects.requireNonNull(getActivity())).getContactList();
        RecyclerView recyclerView = rootView.findViewById(R.id.personRecyclerView);
        ContactsAdapter adapter = new ContactsAdapter(this, contactList);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setAdapter(adapter);

        return rootView;

    }

    public void selectItemAndNavigate(Person person) {
        ((MainActivity) getActivity()).navigateToDetails(person);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void onClick(View view) {
        Fragment fragment = null;
        if(view.getId() == R.id.addNewContactButton) {
            fragment = AddNewFragment.newInstance();
            ((MainActivity) Objects.requireNonNull(getActivity())).loadFragment(
                    fragment,
                    "addnew",
                    false
            );
        }
    }

}