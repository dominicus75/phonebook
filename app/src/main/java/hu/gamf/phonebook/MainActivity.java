package hu.gamf.phonebook;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    private Person selectedPerson;
    private List<Person> contactList = ContactManager.getContactList();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        loadFragment(ContactListFragment.newInstance(),"contacts", false);
    }

    public void loadFragment(Fragment fragment, String tag, boolean addToBackStack) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fragmentContainer, fragment, tag);

        if (addToBackStack) {
            fragmentTransaction.addToBackStack(tag);
        }
        fragmentTransaction.commit();
    }

    public void navigateToDetails(Person person) {
        //Toast.makeText(this, movie.getTitle(), Toast.LENGTH_SHORT).show();
        selectedPerson = person;
        loadFragment(PersonDetailsFragment.newInstance(), "person", true);
    }

    public Person getSelectedPerson() {
        return selectedPerson;
    }

    public List<Person> getContactList() { return contactList; }

    public void addContact(
            String firstName,
            String lastName,
            String phone,
            String email
    ) {
        Person person = new Person(
                firstName,
                lastName,
                phone,
                email,
                "ic_default_avatar"
        );

        if(contactList.add(person)) {
            selectedPerson = person;
        }

    }


}