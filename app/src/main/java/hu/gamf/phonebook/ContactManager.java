package hu.gamf.phonebook;

import java.util.ArrayList;
import java.util.List;

public class ContactManager {

    public static List<Person> getContactList() {
        List<Person> contactList = new ArrayList<>();
        contactList.add(new Person("Pötyi","Macska", "3612345678", "potyi@gmail.com", "ic_cat"));
        contactList.add(new Person("Gipsz","Jakab", "3618765432", "gipsz.jakab@gmail.com", "ic_gipsz_jakab"));
        contactList.add(new Person("Ló","Jenő", "3612586347", "lo.jeno@gmail.com", "ic_lo_jeno"));
        contactList.add(new Person("Macska","János", "3617892568", "macska.janos@gmail.com", "ic_macska_janos"));
        return contactList;
    }

}
